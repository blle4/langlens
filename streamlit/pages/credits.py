import streamlit as st

st.title("Credits")
st.write("Durch die Nutzung von Daten und nutzerorientierten Instrumenten können wir die nächste Revolution auf dem Weg zur Beendigung häuslicher Gewalt einleiten, indem wir Mitarbeitern an vorderster Front die Mittel an die Hand geben, Gewalt zu erkennen, vorherzusagen und zu verhindern, bevor sie geschieht.")